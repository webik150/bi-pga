modes = {
    move: "move",
    edit: "edit",
}

currentMap = {
    style: {
        strokeColor: "#3f3f3f",
        fillColor: "#fff",
        textColor: "#000",
        fontSize: 20,
    },
    title: "",
    thoughts: [],
    connections: [],
    deltaX: 0,
    deltaY: 0,
    currentMode: modes.edit,
    moveInterval: undefined,
    lastMousePosX: 0,
    lastMousePosY: 0,
    moving: false,
}



window.onload = function () {
    document.documentElement.style.width = "100%";
    document.documentElement.style.height = "100%";
    document.body.style.backgroundColor = "#333333";
    document.body.style.width = "100%";
    document.body.style.height = "100%";
    document.body.style.margin = "0";
    this.loadStyles();
    this.buildCanvas();
    this.buildInteractionUI();
    this.updateCanvasSettings();
};

window.onresize = function () {
    let canv = document.getElementById("canvas");
    canv.width = window.innerWidth;
    canv.height = window.innerHeight;
    let ctx = canv.getContext("2d");
    this.updateCanvasSettings();
    this.clear(ctx);
    this.redraw(ctx);
}

document.onkeydown = this.keyboardHandler;

function onCanvasClick(e) {
    if (currentMap.currentMode === modes.edit) {
        let canv = document.getElementById("canvas");
        let ctx = canv.getContext("2d");
        addThought(e.clientX, e.clientY, ctx);
    }
}

function mouseMove(e) {
    if (currentMap.currentMode === modes.move && currentMap.moving) {
        if (currentMap.lastMousePosX === undefined) {
            currentMap.lastMousePosX = e.clientX;
        }
        if (currentMap.lastMousePosY === undefined) {
            currentMap.lastMousePosY = e.clientY;
        }
        pan(e.clientX - currentMap.lastMousePosX, e.clientY - currentMap.lastMousePosY);
        currentMap.lastMousePosY = e.clientY;
        currentMap.lastMousePosX = e.clientX;
    }
}

function startMove(e) {
    
    currentMap.moving = true;
    currentMap.lastMousePosX = e.clientX;
    currentMap.lastMousePosY = e.clientY;
}

function endMove(e) {
    currentMap.moving = false;
}

function addThought(x, y, ctx) {
    let thought = currentMap.thoughts.push({
        id: currentMap.thoughts.length,
        text: [],
        x: x - currentMap.deltaX,
        y: y - currentMap.deltaY,
        width: 0,
        height: 0,
    });
    changeText(thought, ctx, "ye aa yy");
    redraw(ctx);
}

function changeText(id, ctx, text) {
    id -= 1;
    currentMap.thoughts[id].text = smartStringSplit(text);
    let maxLen = 0;
    let maxId = 0;
    for (i = 0; i < currentMap.thoughts[id].text.length; i++) {
        if (currentMap.thoughts[id].text[i].length > maxLen) {
            maxLen = currentMap.thoughts[id].text[i].length;
            maxId = i;
        }
    }

    let measurement = ctx.measureText(currentMap.thoughts[id].text[maxId]);
    currentMap.thoughts[id].width = measurement.width - (measurement.width / 4);
    currentMap.thoughts[id].height = currentMap.thoughts[id].text.length * (currentMap.style.fontSize + 2);
}

function pan(x, y) {
    let canv = document.getElementById("canvas");
    let ctx = canv.getContext("2d");
    clear(ctx);
    currentMap.deltaX += x;
    currentMap.deltaY += y;
    redraw(ctx);
}

function redraw(ctx) {
    currentMap.thoughts.forEach(thought => {
        ctx.strokeStyle = currentMap.style.strokeColor;
        ctx.fillStyle = currentMap.style.fillColor;
        ctx.beginPath();
        ctx.ellipse(thought.x + currentMap.deltaX, thought.y + currentMap.deltaY, thought.width, thought.height, 0, 180, 180);
        ctx.stroke();
        ctx.fill();
        ctx.fillStyle = currentMap.style.textColor;
        ctx.beginPath();
        let currentLine = 0;
        let yFix = 0;
        if (thought.text.length > 4) {
            yFix = (thought.height / 4);
        } else if (thought.text.length === 3) {
            yFix = (thought.height / 6);
        }
        else if (thought.text.length === 2) {
            yFix = (thought.height / 8);
        }
        else if (thought.text.length === 1) {
            yFix = -currentMap.style.fontSize / 4;
        }
        thought.text.forEach(line => {
            ctx.fillText(line, thought.x + currentMap.deltaX, thought.y + currentMap.deltaY + (currentLine * (currentMap.style.fontSize + 2) - yFix));
            currentLine++;
        });
    });
}

function updateCanvasSettings() {
    let canv = document.getElementById("canvas");
    let ctx = canv.getContext("2d");
    ctx.lineWidth = 5;
    ctx.textAlign = "center";
    ctx.font = currentMap.style.fontSize + "px sans-serif";
}

function clear(ctx) {
    currentMap.thoughts.forEach(thought => {
        ctx.clearRect(thought.x + currentMap.deltaX - (thought.width + ctx.lineWidth), thought.y + currentMap.deltaY - (thought.height + ctx.lineWidth), (thought.width + ctx.lineWidth) * 2, (thought.height + ctx.lineWidth) * 2);
    });
}

function clearAll(ctx){
    ctx.clearRect(0,0,window.innerWidth,window.innerHeight);
}

/* KEYBOARD EVENTS */

function keyboardHandler(e) {

    let speed = 5;
    let code = e.keyCode;
    if (code == 39) {
        // Right
        pan(speed, 0);
        e.preventDefault();
    }
    if (code == 37) {
        // Left
        pan(-speed, 0);
        e.preventDefault();
    }
    if (code == 40) {
        // Down
        pan(0, speed);
        e.preventDefault();
    }
    if (code == 38) {
        // Up
        pan(0, -speed);
        e.preventDefault();
    }
    if (code == 65) {
        switch (currentMap.currentMode) {
            case modes.move:
                selectMode(modes.edit);
                break;
            case modes.edit:
                selectMode(modes.move);
                break;
            default:
                break;
        }
    }
    if(code == 80){
        printJSON();
    }
    if(code == 79){
        let json = prompt("Please enter JSON mindmap");
        if(json != null){
            loadJSON(json);
        }
    }
}

/* UI BUILDING */

/** Thanks https://stackoverflow.com/a/577002/7886701 */
function loadStyles() {
    let cssId = 'css_link';  // you could encode the css path itself to generate id..
    if (!document.getElementById(cssId)) {
        let head = document.getElementsByTagName('head')[0];
        let link = document.createElement('link');
        link.id = cssId;
        link.rel = 'stylesheet';
        link.type = 'text/css';
        link.href = './style.css';
        link.media = 'all';
        head.appendChild(link);
    }
    cssId = 'css_link_icons';  // you could encode the css path itself to generate id..
    if (!document.getElementById(cssId)) {
        let head = document.getElementsByTagName('head')[0];
        let link = document.createElement('link');
        link.id = cssId;
        link.rel = 'stylesheet';
        link.type = 'text/css';
        link.href = "https://fonts.googleapis.com/icon?family=Material+Icons";
        link.media = 'all';
        head.appendChild(link);
    }
}

function buildCanvas() {
    let canv = document.createElement("canvas");
    canv.width = window.innerWidth;
    canv.height = window.innerHeight;
    canv.id = "canvas";
    document.body.appendChild(canv);
    canv.addEventListener("click", this.onCanvasClick);
    canv.addEventListener("mousemove", this.mouseMove);
    canv.addEventListener("mousedown", this.startMove);
    canv.addEventListener("mouseup", this.endMove);
}

function buildInteractionUI() {
    let menu = document.createElement("div");
    menu.id = "menu";
    document.body.appendChild(menu);
    let buttons = [];
    let icons = ["open_with", "edit", "save", "folder_open", "delete_forever"];
    var menuButtons = ["move", "edit", "save", "new", "clear"];
    for (let index = 0; index < 5; index++) {
        buttons.push(document.createElement("button"));
        let icon = document.createElement("i");
        icon.innerHTML = icons[index];
        icon.classList.add("material-icons");
        buttons[index].appendChild(icon)
        buttons[index].id = menuButtons[index]
        buttons[index].addEventListener("click",function(){
            selectMode(menuButtons[index])
        })
        menu.appendChild(buttons[index]);
    }
    selectMode(currentMap.currentMode);
}

function selectMode(mode){
    document.getElementById(this.currentMap.currentMode).removeAttribute("selected");
    document.getElementById("canvas").setAttribute("mode",mode);
    document.getElementById(mode).setAttribute("selected","");
    this.currentMap.currentMode = mode;
}

/* SAVING AND LOADING */

function loadJSON(json){
    let newMap = JSON.parse(json);
    resetCurrentMap();
    currentMap.title = newMap.title;
    currentMap.thoughts = newMap.thoughts;
    currentMap.connections = newMap.connections;
    currentMap.style = newMap.style;
    let canv = document.getElementById("canvas");
    let ctx = canv.getContext("2d");
    updateCanvasSettings();
    clearAll(ctx);
    redraw(ctx);
}

function saveJSON(){
    let json = JSON.stringify({
        title: currentMap.title,
        thoughts: currentMap.thoughts,
        connections: currentMap.connections,
        style: currentMap.style
        });
    return json;
}

/* HELPER FUNCTIONS */

function resetCurrentMap(){
    currentMap.title = "";
    currentMap.thoughts = [];
    currentMap.connections = [];
    currentMap.deltaX = 0;
    currentMap.deltaY = 0;
    currentMap.lastMousePosX = 0;
    currentMap.lastMousePosY = 0;
}

function smartStringSplit(text) {
    let words = text.split(" ");
    let ratio = text.length / words.length;
    let maxLen = 0;
    for (let index = 0; index < Math.ceil(words.length / ratio); index++) {
        maxLen += words[index].length + 1;
    }
    let newWords = [""];
    let currentLen = 0;
    words.forEach(word => {
        currentLen += word.length;
        if (currentLen > maxLen) {
            newWords[newWords.length - 1] = newWords[newWords.length - 1].slice(0, -1);
            newWords.push("");
            currentLen = 0;
        }
        newWords[newWords.length - 1] += word + " ";
    });
    newWords[newWords.length - 1] = newWords[newWords.length - 1].slice(0, -1);
    return newWords;
}

function printJSON(){
    console.log(saveJSON());
}