//
// SAGE2 application: mindmap
// by: Krystof Jezek <webik150@xenonlabs.eu>
//
// Copyright (c) 2019
//

"use strict";

/* global  */


var mindmap = SAGE2_App.extend({
	init: function (data) {
		// Create div into the DOM
		this.SAGE2Init("div", data);
		// Set the DOM id
		this.element.id = "div_" + data.id;
		// Set the background to black
		this.element.style.backgroundColor = 'black';

		// move and resize callbacks
		this.resizeEvents = "continuous"; // onfinish

		this.modes = {
			move: "move",
			add: "add",
			select: "select",
			save: "save",
			open: "open",
			clear: "clear"
		}

		// SAGE2 Application Settings
		//
		// Control the frame rate for an animation application
		//this.maxFPS = 2.0;
		// Not adding controls but making the default buttons available
		this.enableControls = true;
		this.state.currentFontSize = this.state.style.fontSize * this.state.zoom;
		this.loadStyles();
		this.buildCanvas();
		this.buildInteractionUI();
		this.updateCanvasSettings();
		this.selectMode(this.state.currentMode);
		this.clearAll();
		this.redraw();
		this.refresh(data.date);
	},

	load: function (date) {
		console.log('mindmap> Load with state value', this.state.value);
		this.updateCanvasSettings();
		this.selectMode(this.state.currentMode);
		this.clearAll();
		this.redraw();
		this.refresh(date);
	},

	draw: function (date) {},

	/**
	 * Called when resizing the app window. Updates the canvas.
	 * @param {*} date 
	 */
	resize: function (date) {
		// Called when window is resized
		this.canvas.width = this.sage2_width;
		this.canvas.height = this.sage2_height;
		if (this.currentModeLabel != null) {
			fitText(this.currentModeLabel, 0.3);
		}
		this.updateCanvasSettings();
		this.clearAll();
		this.redraw();
		this.refresh(date);
	},

	move: function (date) {
		// Called when window is moved (set moveEvents to continuous)
		this.refresh(date);
	},

	quit: function () {
		// Make sure to delete stuff (timers, ...)
	},

	/* CANVAS STUFF */
	/**
	 * Adds a new thought at specified coordinates
	 * @param {float} x 
	 * @param {float} y 
	 */
	addThought: function (x, y) {
		//console.log("Adding thought to",x,y, this.state)
		this.state.maxID++;
		let thought = this.state.thoughts.push({
			id: this.state.thoughts.length,
			text: [],
			x: x - this.state.deltaX,
			y: y - this.state.deltaY,
			width: 0,
			height: 0,
		}) - 1;
		if (this.state.selected !== -1) {
			this.state.connections.push({
				label: "",
				a: Math.min(this.state.selected, thought),
				b: Math.max(this.state.selected, thought),
			})
		}
		this.changeText(thought, thought.toString());
		this.redraw();
		this.SAGE2UpdateAppOptionsFromState();
		this.SAGE2Sync(true);
		this.getFullContextMenuAndUpdate();
	},

	/**
	 * Removes the currently selected thought.
	 */
	removeSelected: function(){
		this.state.thoughts.splice(this.state.selected, 1);
		for (let i = 0; i < this.state.thoughts.length; i++) {
			if (this.state.thoughts[i].id > this.state.selected) {
				this.state.thoughts[i].id -= 1;
			}

		}
		for (let i = this.state.connections.length - 1; i >= 0; i--) {
			if (this.state.connections[i].a === this.state.selected || this.state.connections[i].b === this.state.selected) {
				this.state.connections.splice(i, 1);
			}
		}
		for (let i = this.state.connections.length - 1; i >= 0; i--) {
			if (this.state.connections[i].a > this.state.selected) {
				this.state.connections[i].a -= 1;
			}
			if (this.state.connections[i].b > this.state.selected) {
				this.state.connections[i].b -= 1;
			}
		}
		this.state.selected = -1;
		this.clearAll();
		this.redraw();
		this.SAGE2Sync(true);
		
	},

	/**
	 * Changes the text of a thought and automatically resizes the thought.
	 * @param {int} id 
	 * @param {string} text 
	 */
	changeText: function (id, text) {
		this.state.thoughts[id].text = this.smartStringSplit(text);
		let maxLen = 0;
		let maxId = 0;
		for (let i = 0; i < this.state.thoughts[id].text.length; i++) {
			if (this.state.thoughts[id].text[i].length > maxLen) {
				maxLen = this.state.thoughts[id].text[i].length;
				maxId = i;
			}
		}
		let measurement = this.context.measureText(this.state.thoughts[id].text[maxId]);
		this.state.thoughts[id].height = this.state.thoughts[id].text.length * (this.state.currentFontSize + 2);
		this.state.thoughts[id].width = Math.max(measurement.width - (measurement.width / 4), this.state.thoughts[id].height);
	},

	/**
	 * Pans the entire mindmap by relative X,Y
	 * @param {float} x 
	 * @param {float} y 
	 */
	pan: function (x, y) {
		this.clearAll();
		this.state.deltaX += x;
		this.state.deltaY += y;
		this.redraw();
	},

	/**
	 * Draws all the thoughts and connections
	 */
	redraw: function () {
		this.state.connections.forEach(line => {
			this.context.strokeStyle = this.state.style.strokeColor;
			this.context.beginPath();
			let from = this.state.thoughts[line.a];
			let to = this.state.thoughts[line.b];
			this.context.moveTo(from.x + this.state.deltaX, from.y + this.state.deltaY)
			this.context.lineTo(to.x + this.state.deltaX, to.y + this.state.deltaY)
			this.context.stroke();
		});

		this.state.thoughts.forEach(thought => {
			if (thought.id === this.state.selected) {
				this.context.lineWidth += 3;
				this.context.beginPath();
				this.context.strokeStyle = "red";
				this.context.ellipse(thought.x + this.state.deltaX, thought.y + this.state.deltaY, thought.width, thought.height, 0, 180, 180);
				this.context.stroke();
				this.context.lineWidth -= 3;
			}
			this.context.strokeStyle = this.state.style.strokeColor;
			this.context.fillStyle = this.state.style.fillColor;
			this.context.beginPath();
			this.context.ellipse(thought.x + this.state.deltaX, thought.y + this.state.deltaY, thought.width, thought.height, 0, 180, 180);
			this.context.stroke();
			this.context.fill();
			this.context.fillStyle = this.state.style.textColor;
			this.context.beginPath();
			let currentLine = 0;
			let yFix = 0;
			if (thought.text.length > 4) {
				yFix = (thought.height / 4);
			} else if (thought.text.length === 3) {
				yFix = (thought.height / 6);
			}
			else if (thought.text.length === 2) {
				yFix = (thought.height / 8);
			}
			else if (thought.text.length === 1) {
				yFix = -this.state.currentFontSize / 4;
			}
			thought.text.forEach(line => {
				this.context.fillText(line, thought.x + this.state.deltaX, thought.y + this.state.deltaY + (currentLine * (this.state.currentFontSize + 2) - yFix));
				currentLine++;
			});
		});
	},

	/**
	 * Updates the canvas colors and sizes.
	 */
	updateCanvasSettings: function () {
		this.context.lineWidth = 5 * this.state.zoom;
		this.context.textAlign = "center";
		this.context.font = this.state.currentFontSize + "px sans-serif";
		this.canvas.style.backgroundColor = this.state.style.canvasColor;
	},

	/**
	 * Clears the whole canvas.
	 */
	clearAll: function () {
		this.context.clearRect(0, 0, this.sage2_width, this.sage2_height);
	},

	/**
	 * Updates the sizes of all thoughts to fit current font size.
	 */
	updateFontSize: function () {
		this.state.thoughts.forEach(thought => {
			this.changeText(thought.id, thought.text.join(" "));
		});
		this.SAGE2Sync(true);
		this.getFullContextMenuAndUpdate();
	},

	/* EVENTS */
	/**
	 * SAGE2 event callback.
	 */
	event: function (eventType, position, user_id, data, date) {
		if (eventType === "pointerPress" && (data.button === "left")) {
			let out = this.isInThought(position);
			switch (this.state.currentMode) {
				case this.modes.move:
					this.state.moving = true;
					this.state.lastMousePosX = position.x;
					this.state.lastMousePosY = position.y;
					if (out.hit) {
						this.state.selected = out.id;
					} else {
						this.state.selected = -1;
					}
					this.SAGE2Sync(true);
					this.clearAll();
					this.redraw();
					break;
				case this.modes.add:
					if (!out.hit) {
						this.addThought(position.x, position.y);
					}
					break;
				case this.modes.select:
					if (out.hit) {
						this.state.selected = out.id;
					} else {
						this.state.selected = -1;
					}
					this.SAGE2Sync(true);
					this.clearAll();
					this.redraw();
					break;
				default:
					break;
			}
			this.refresh(date);
		} else if (this.state.currentMode === this.modes.move && eventType === "pointerMove" && this.state.moving) {
			if (this.state.lastMousePosX === undefined) {
				this.state.lastMousePosX = position.x;
			}
			if (this.state.lastMousePosY === undefined) {
				this.state.lastMousePosY = position.y;
			}
			const ray = this.isInThought(position);
			if (!ray.hit && this.grabbed === -1) {
				this.pan(position.x - this.state.lastMousePosX, position.y - this.state.lastMousePosY);
			} else {
				if (this.grabbed === -1) {
					this.grabbed = ray.id;
				}
				this.state.thoughts[this.grabbed].x += (position.x - this.state.lastMousePosX);
				this.state.thoughts[this.grabbed].y += (position.y - this.state.lastMousePosY);
				this.clearAll();
				this.redraw();
				this.SAGE2Sync(true);
			}
			this.state.lastMousePosY = position.y;
			this.state.lastMousePosX = position.x;
		} else if (eventType === "pointerRelease" && (data.button === "left")) {
			this.grabbed = -1;
			this.state.moving = false;
		} else if (eventType === "pointerScroll") {			// Scroll events for zoom
			this.state.zoom -= data.wheelDelta / 1000.0;
			if (this.state.zoom <= 0.1) {
				this.state.zoom = 0.1;
			}
			this.state.currentFontSize = this.state.style.fontSize * this.state.zoom;
			this.SAGE2Sync(true);
			this.updateCanvasSettings();
			this.updateFontSize();
			this.clearAll();
			this.redraw();
			
		} else if (eventType === "keyboard") {							// KEYBOARD
			if (data.character === "h") {
				this.toggleUI(date);
			} else if (data.character === "a") {
				this.selectMode(this.modes.add);
			} else if (data.character === "s") {
				this.selectMode(this.modes.select);
			} else if (data.character === "d") {
				this.selectMode(this.modes.move);
			} else if (data.character === "p") {
				this.printJSON();
			} else if (data.character === "c") {
				this.selectMode(this.modes.clear);
			} else if (data.character === "+") {					// Zooming
				this.state.zoom += 0.1;
				this.state.currentFontSize = this.state.style.fontSize * this.state.zoom;
				this.SAGE2Sync(true);
				this.updateCanvasSettings();
				this.updateFontSize();
				this.clearAll();
				this.redraw();
			} else if (data.character === "-") {
				if (this.state.zoom <= 0.1) {
					this.state.zoom = 0.1;
				}
				this.state.zoom -= 0.1;
				this.state.currentFontSize = this.state.style.fontSize * this.state.zoom;
				this.SAGE2Sync(true);
				this.updateCanvasSettings();
				this.updateFontSize();
				this.clearAll();
				this.redraw();
			}
		} else if (eventType === "specialKey") {						// SPECIAL KEYS
			if (data.code === 37 && data.state === "down") {
				// left
				this.pan(-5, 0);
				this.refresh(date);
			} else if (data.code === 38 && data.state === "down") {
				// up
				this.pan(0, -5);
				this.refresh(date);
			} else if (data.code === 39 && data.state === "down") {
				// right
				this.pan(5, 0);
				this.refresh(date);
			} else if (data.code === 40 && data.state === "down") {
				// down
				this.pan(0, 5);
				this.refresh(date);
			}
		} else if (eventType === "widgetEvent") {						// WIDGETS
			switch (data.identifier) {
				case "thought_text":
					if (this.state.selected !== -1) {
						this.changeText(this.state.selected, data.text)
						this.clearAll();
						this.redraw();
						this.getFullContextMenuAndUpdate();
					}
					break;
				case "AddMode":
					this.selectMode(this.modes.add);
					break;
				case "SelectMode":
					this.selectMode(this.modes.select);
					break;
				case "MoveMode":
					this.selectMode(this.modes.move);
					break;
				case "ClearButton":
					this.selectMode(this.modes.clear);
					break;
				case "HideButton":
					this.toggleUI();
					break;
				default:
					break;
			}
		}
	},

	/* UI */
	/**
	 * Toggle the application UI.
	 * @param {*} date 
	 */
	toggleUI: function (date) {
		this.state.uiVisible = !this.state.uiVisible;
		if (this.menu != null) {
			if (this.state.uiVisible) {
				this.menu.setAttribute("hidden", "")
			} else {
				this.menu.removeAttribute("hidden")
			}
		}
		if (this.currentModeLabel != null) {
			if (this.state.uiVisible) {
				this.currentModeLabel.setAttribute("hidden", "")
			} else {
				this.currentModeLabel.removeAttribute("hidden")
			}
		}
		this.refresh(date);
	},

	/**
	 * Loads the necessary css into head.
	 */
	loadStyles: function () {
		let cssId = 'mindmap-css';  // you could encode the css path itself to generate id..
		if (!document.getElementById(cssId)) {
			let head = document.getElementsByTagName('head')[0];
			let link = document.createElement('link');
			link.id = cssId;
			link.rel = 'stylesheet';
			link.type = 'text/css';
			link.href = 'css/mindmap.css';
			link.media = 'all';
			head.appendChild(link);
		}
		//unused
		/*cssId = 'mindmap-css-icons';  // you could encode the css path itself to generate id..
		if (!document.getElementById(cssId)) {
			let head = document.getElementsByTagName('head')[0];
			let link = document.createElement('link');
			link.id = cssId;
			link.rel = 'stylesheet';
			link.type = 'text/css';
			link.href = "https://fonts.googleapis.com/icon?family=Material+Icons";
			link.media = 'all';
			head.appendChild(link);
		}*/

	},

	/**
	 * Creates the canvas and initializes app-wide variables canvas and context.
	 */
	buildCanvas: function () {
		let canv = document.createElement("canvas");
		canv.width = this.sage2_width;
		canv.height = this.sage2_height;
		canv.classList.add("mindmap-canvas");
		this.element.appendChild(canv);
		this.canvas = canv;
		this.context = canv.getContext("2d");
	},

	/**
	 * Builds the widget menu + html UI (current mode labels and such)
	 */
	buildInteractionUI: function () {
		// Html
		let wrapper = document.createElement("div");
		wrapper.classList.add("mindmap-mode-wrapper");
		this.element.appendChild(wrapper);
		this.labels = {};
		for (const key in this.modes) {
			if (this.modes.hasOwnProperty(key)) {
				const element = this.modes[key];
				this.labels[key] = document.createElement("div");
				this.labels[key].innerHTML = element;
				this.labels[key].classList.add("mindmap-mode-label")
				wrapper.appendChild(this.labels[key]);
				fitText(this.labels[key], 1);
				if (key != this.state.currentMode) {
					this.labels[key].style.display = "none";
				}
			}
		}
		wrapper = document.createElement("div");
		wrapper.classList.add("mindmap-current-mode-wrapper");
		this.element.appendChild(wrapper);
		this.currentModeLabel = document.createElement("div");
		this.currentModeLabel.innerHTML = this.state.currentMode[0];
		wrapper.appendChild(this.currentModeLabel);
		fitText(this.currentModeLabel, 0.3);

		// Widgets
		this.controls.addTextInput({ defaultText: "", label: "Text", identifier: "thought_text" });

		let button = {
			"state": 0,
			"width": 24,
			"height": 24,
			"fill": "none",
			"strokeWidth": 1,
			"delay": 600,
			"textual": false,
			"animation": false
		};

		this.controls.addButtonType("mindmapButton", button);
		// UNFORTUNATELY, I wasn't able to add icons to the buttons in the end, so labels will have to do.
		this.controls.addButton({ type: "mindmapButton", position: 3, label: "ADD", identifier: "AddMode" });
		this.controls.addButton({ type: "mindmapButton", position: 2, label: "SEL", identifier: "SelectMode" });
		this.controls.addButton({ type: "mindmapButton", position: 1, label: "MOV", identifier: "MoveMode" });
		this.controls.addButton({ type: "mindmapButton", position: 8, label: "CLR", identifier: "ClearButton" });

		this.controls.finishedAddingControls();
	},

	/**
	 * Returns an object {hit:bool, id:int}, where hit specifies if any thought was hit, and id specifies the thought that was hit.
	 */
	isInThought: function (position) {
		let result = { hit: false, id: -1 }
		for (let index = 0; index < this.state.thoughts.length; index++) {
			const thought = this.state.thoughts[index];
			let test = Math.pow(position.x - (this.state.deltaX + thought.x), 2) / Math.pow(thought.width, 2)
				+ Math.pow(position.y - (this.state.deltaY + thought.y), 2) / Math.pow(thought.height, 2);
			if (test <= 1) {
				result.hit = true;
				result.id = index;
			}
		}
		return result;
	},

	/**
	 * Changes the current mode.
	 * @param {string} mode - mode to switch to. Has to be one of "this.modes"
	 */
	selectMode: function (mode) {
		if (mode === this.modes.clear) {
			if (this.state.selected !== -1) {
				this.removeSelected();
				return;
			} else {
				// Clear all and then switch back to last mode.
				const prev = this.state.currentMode;
				this.labels[this.state.currentMode].style.display = "none";
				this.state.currentMode = mode;
				this.SAGE2Sync(true);
				this.labels[this.state.currentMode].style.display = "block";
				this.labels[this.state.currentMode].classList.remove("mindmap-mode-label");
				this.labels[this.state.currentMode].classList.add("mindmap-mode-label");
				fitText(this.labels[this.state.currentMode], 1);
				this.resetCurrentMap();
				this.clearAll();
				this.getFullContextMenuAndUpdate();
				const _this = this;
				(function (prev, _this) {
					setTimeout(function () {
						_this.selectMode(prev);
					}, 600)
				})(prev, _this);
				return;
			}

		}
		this.labels[this.state.currentMode].style.display = "none";
		this.state.currentMode = mode;
		if (this.canvas != null) {
			this.canvas.setAttribute("mode", this.state.currentMode);
		}
		this.currentModeLabel.innerHTML = this.state.currentMode[0];
		this.SAGE2Sync(true);
		this.labels[this.state.currentMode].style.display = "block";
		// Retrigger animation
		this.labels[this.state.currentMode].classList.remove("mindmap-mode-label");
		this.labels[this.state.currentMode].classList.add("mindmap-mode-label");
		fitText(this.labels[this.state.currentMode], 1);
		console.log("mindmap> Changed mode to: ",this.state.currentMode);
	},


	/* SAVING AND LOADING */
	/**
	 * Try parsing data as a mindmap
	 * @param {string} json - JSON to load
	 */
	loadJSON: function (json) {
		try {
			let newMap = JSON.parse(json);
			this.resetCurrentMap(this);
			this.state.title = newMap.title;
			this.state.thoughts = newMap.thoughts;
			this.state.connections = newMap.connections;
			this.state.style = newMap.style;
			this.SAGE2Sync(true);
			this.updateCanvasSettings();
			this.clearAll();
			this.redraw();
		} catch (ex) {
			console.error("mindmap> Unable to parse map. Wrong format.")
			return;
		}
		console.log("mindmap> Parsed map:", e.clientInput);
	},

	/**
	 * Try parsing data entered in context menu
	 * @param {*} e - event object
	 */
	parseJSON: function (e) {
		this.loadJSON(e.clientInput);
	},

	/**
	 * Returns current map in form of JSON
	 */
	saveJSON: function () {
		this.state.title = "a";
		this.SAGE2Sync(true);
		let json = JSON.stringify({
			title: this.state.title,
			thoughts: this.state.thoughts,
			connections: this.state.connections,
			style: this.state.style
		});
		console.log("mindmap> Saving to ", json);
		return json;
	},

	/* MISC */

	/**
	 * Resets current map to initial empty state
	 */
	resetCurrentMap: function () {
		this.state.title = "";
		this.state.thoughts = [];
		this.state.connections = [];
		this.state.deltaX = 0;
		this.state.deltaY = 0;
		this.state.lastMousePosX = 0;
		this.state.lastMousePosY = 0;
		this.SAGE2Sync(true);
	},

	/**
	 * Split the provided text into lines in a semi-smart way.
	 * @param {string} text 
	 */
	smartStringSplit: function (text) {
		let words = text.split(" ");
		let ratio = text.length / words.length;
		let maxLen = 0;
		for (let index = 0; index < Math.ceil(words.length / ratio); index++) {
			maxLen += words[index].length + 1;
		}
		let newWords = [""];
		let currentLen = 0;
		words.forEach(word => {
			currentLen += word.length;
			if (currentLen > maxLen) {
				newWords[newWords.length - 1] = newWords[newWords.length - 1].slice(0, -1);
				newWords.push("");
				currentLen = 0;
			}
			newWords[newWords.length - 1] += word + " ";
		});
		newWords[newWords.length - 1] = newWords[newWords.length - 1].slice(0, -1);
		return newWords;
	},

	/**
	 * Debug function for printing current map into console.
	 */
	printJSON: function () {
		console.log(this.saveJSON());
	},

	//Color changing
	/**
	 * Changes the fill color of thoughts on the mind map
	 * @param {*} e - the event object
	 */
	changeFillColor: function (e) {
		this.state.style.fillColor = e.clientInput;
		this.SAGE2Sync(true);
		this.updateCanvasSettings();
		this.redraw();
		this.getFullContextMenuAndUpdate();
	},
	/**
	 * Changes the color of text on the mind map
	 * @param {*} e - the event object
	 */
	changeTextColor: function (e) {
		this.state.style.textColor = e.clientInput;
		this.SAGE2Sync(true);
		this.updateCanvasSettings();
		this.redraw();
		this.getFullContextMenuAndUpdate();
	},
	/**
	 * Changes the background color of the mind map
	 * @param {*} e - the event object
	 */
	changeBackgroundColor: function (e) {
		this.state.style.canvasColor = e.clientInput;
		this.SAGE2Sync(true);
		this.updateCanvasSettings();
		this.redraw();
		this.getFullContextMenuAndUpdate();
	},
	/**
	 * Changes the stroke color of items on the mind map
	 * @param {*} e - the event object
	 */
	changeStrokeColor: function (e) {
		this.state.style.strokeColor = e.clientInput;
		this.SAGE2Sync(true);
		this.updateCanvasSettings();
		this.redraw();
		this.getFullContextMenuAndUpdate();
	},

	/**
	 * SAGE2 uses this function to fill up the context menu of the app.
	 */
	getContextEntries: function () {
		var entries = [];
		let entry = {
			description: "Copy map to clipboard",
			callback: "SAGE2_copyURL",
			parameters: { url: this.saveJSON() },
			entryColor: "lightpink"
		}
		entries.push(entry);

		entry = {
			description: "Parse map: ", // A string
			callback: "parseJSON", // Name of an existing function within this application. Must be in a string.
			parameters: {}, // An object that will be passed to the callback function
			entryColor: "lightBlue", // OPTIONAL: string that will alter the entry color. Can be any of the valid color names recognized by browser or a valid hex string.
			inputField: true, // OPTIONAL: Set to true to allow text input
			inputFieldSize: 20, // OPTIONAL: Integer to specify input field size
			value: "", // OPTIONAL: The input area will be filled with this if specified

		};
		entries.push(entry);

		entry = { description: "separator" }; // This creates a line entry used for visual separation
		entries.push(entry);

		// Set Color entries
		entry = {
			description: "Set Background Color:",
			callback: "changeBackgroundColor",
			parameters: {},
			entryColor: "lightGreen",
			inputField: true,
			inputFieldSize: 8,
			value: this.state.style.canvasColor,

		};
		entries.push(entry);

		entry = {
			description: "Set Fill Color:",
			callback: "changeFillColor",
			parameters: {},
			entryColor: "lightGreen",
			inputField: true,
			inputFieldSize: 8,
			value: this.state.style.fillColor,

		};
		entries.push(entry);

		entry = {
			description: "Set Text Color:",
			callback: "changeTextColor",
			parameters: {},
			entryColor: "lightGreen",
			inputField: true,
			inputFieldSize: 8,
			value: this.state.style.textColor,

		};
		entries.push(entry);

		entry = {
			description: "Set Stroke Color:",
			callback: "changeStrokeColor",
			parameters: {},
			entryColor: "lightGreen",
			inputField: true,
			inputFieldSize: 8,
			value: this.state.style.strokeColor,

		};
		entries.push(entry);

		return entries;
	},
});
