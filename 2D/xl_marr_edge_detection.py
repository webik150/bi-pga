#!/usr/bin/env python
# Krystof Jezek
# Edge detector based on Marr-Hildreth algorithm

import math
from gimpfu import *
import array
import numpy
import scipy.ndimage


def marr_edge_detector(timg, tdrawable, gauss_blur, treshold, desaturate_mode, size, blur_only):
	"""An image detector based on Marr-Hildreth"""

	pdb.gimp_image_undo_group_start(timg)

	# Get necessary variables
	bpp = tdrawable.bpp
	(bx1, by1, bx2, by2) = tdrawable.mask_bounds
	width = bx2 - bx1
	height = by2 - by1

	# Apply gauss blur
	pdb.plug_in_gauss_rle2(timg, tdrawable, gauss_blur, gauss_blur)

	# Debug option to only apply blur
	if blur_only:
		pdb.gimp_image_undo_group_end(timg)
		return

	# Desaturate the layer if it's not already gray.
	if timg.base_type is not GRAY:
		pdb.gimp_desaturate_full(tdrawable, desaturate_mode)

	# Begin the actual process. Process the image in blocks of set size to save memory.
	# Length of one side of the block we'll process.
	# **THEORETICALLY**, if You drastically reduced the amount of available memory after opening the GUI and before executing the plugin,
	# python could run out of memory. This is solved by the min().
	block_options = get_available_memory_options()
	BLOCK_SIZE = int(block_options[min(size, len(block_options) - 1)])
	# Calculate how many pixel blocks we'll need
	blocks_needed_x = int(math.ceil(float(width) / BLOCK_SIZE))
	blocks_needed_y = int(math.ceil(float(height) / BLOCK_SIZE))
	blocks_needed = blocks_needed_x * blocks_needed_y
	# Highest coordinates we could ever want to reach.
	max_x = tdrawable.width
	max_y = tdrawable.height
	# Create a new layer that we'll draw to.
	layer_tmp = tdrawable.copy()
	timg.add_layer(layer_tmp, 0)

	# Process the blocks
	for Y in range(0, blocks_needed_y):
		for X in range(0, blocks_needed_x):
			current_block = (Y * blocks_needed_y) + X
			gimp.progress_init(
				"Processing image block (" + str(current_block) + "/" + str(blocks_needed) + ")")
			gimp.progress_update((current_block) / float(blocks_needed))
			# Start pixels of the pixel block
			cx1 = X * BLOCK_SIZE + bx1
			cy1 = Y * BLOCK_SIZE + by1
			# End pixels of the pixel block
			cx2 = min(cx1 + BLOCK_SIZE, bx2)
			cy2 = min(cy1 + BLOCK_SIZE, by2)
			# Width and height of the pixel block
			cwidth = cx2 - cx1
			cheight = cy2 - cy1
			# d is how thick is the border of each pixel block.
			d = 1
			# Is this block touching the left side of the layer?
			left_side = (cx1 - d < 0)
			# Is this block touching the top side of the layer?
			top_side = (cy1 - d < 0)
			# Final pixel block bounds, including 1 pixel for border if possible.
			x1 = max(cx1 - d, 0)
			y1 = max(cy1 - d, 0)
			# Get the final size. Either blocksize + border, or what remains between the block start and end if drawable.
			final_block_width = min(cx2 + d * 2, max_x) - cx1
			final_block_height = min(cy2 + d * 2, max_y) - cy1
			# Get readonly region of the original image.
			im_rgn = tdrawable.get_pixel_rgn(
				x1, y1, final_block_width, final_block_height, False, False)
			src_pixels = array.array(
				"B", im_rgn[x1:x1 + final_block_width, y1:y1 + final_block_height])
			tmp_pixels = numpy.reshape(
				src_pixels, (final_block_height, final_block_width, bpp)).astype(numpy.uint8)
			tmp_pixels = resize_to_single_channel(
				tmp_pixels, final_block_width, final_block_height)
			# Use scipy to convolve the image using the laplacian kernel.
			tmp_pixels = scipy.ndimage.filters.convolve(
				tmp_pixels, get_laplacian_kernel(), mode='nearest')
			# Find edges.
			edges = zero_cross(
				tmp_pixels, treshold, final_block_width, final_block_height, cwidth, cheight)
			# Write to texture.
			write_to_texture(layer_tmp, edges, cx1 - (1 if not left_side else 0), cx2 - (
				1 if not left_side else 0), cy1 - (1 if not top_side else 0), cy2 - (1 if not top_side else 0))

	# Move the new layer by (1,1) so it's centered
	layer_tmp.translate(1, 1)
	layer_tmp.flush()
	pdb.gimp_image_undo_group_end(timg)
	return


def write_to_texture(tdrawable, pixels_to_write, bx1, bx2, by1, by2):
	"Flattens pixels to 1D array and writes them to the texture"

	# Get data specific for the drawable we want to write to.
	bpp = tdrawable.bpp
	width = bx2 - bx1
	height = by2 - by1

	# Write the result to image - I'm proud of this.
	# Original source (mine is HEAVILY changed): https://gitlab.fit.cvut.cz/BI-PGA/stud/linhaold/blob/master/dokumentace1.adoc
	# Make a new array with the correct amount of channels per pixel and then fill each channel with the found edges. NOTE: shape is now (bpp, height, width)
	tmp_pixels_channels = numpy.zeros([bpp, height, width])
	for i in range(0, bpp):
		tmp_pixels_channels[i] = pixels_to_write[:, :]
	# So basically... Numpy is magic. Instead of iterating through all the pixels and manually changing the shape to the original (height, width, bpp),
	# we just use numpy to reorder the channels internally which is lightspeed fast. (bpp, height, width)->(height, bpp, width)->(height, width, bpp)
	tmp_pixels_channels = numpy.swapaxes(
		numpy.swapaxes(tmp_pixels_channels, 0, 1), 1, 2)
	# Convert from 3D numpy float array to 1D python int array
	flattenedpixels = array.array(
		"B", tmp_pixels_channels.astype(numpy.int).flatten())
	# Get an EDITABLE pixel region and then apply the new pixels.
	dest_rgn = tdrawable.get_pixel_rgn(bx1, by1, width, height)
	dest_rgn[bx1:bx2, by1:by2] = flattenedpixels.tostring()


def get_laplacian_kernel():
	"""Returns the laplacian kernel

	Source: https://homepages.inf.ed.ac.uk/rbf/HIPR2/log.htm
	"""

	return numpy.array([
		[0, -1,	0],
		[-1, 4, -1],
		[0, -1,	0]])


def resize_to_single_channel(pixels, width, height):
	"""Turns a multiple channel array into a single channel one. (We don't need more since the image is grayscale)"""

	return pixels[:, :, 0].astype(numpy.float)


def zero_cross(pixels, treshold, width, height, core_width, core_height):
	"""Finds all the sign crossings in supplied array and sets them to white"""

	# Create a new array for the result
	edges = numpy.zeros((core_height, core_width))
	# value := how bright should the edges be (0=black, 255=white)
	# d := delta/block center offset
	value = 255
	d = 1
	for y in range(d, height - d):
		for x in range(d, width - d):
			# Sources:
			# https://gitlab.fit.cvut.cz/BI-PGA/stud/linhaold/blob/master/dokumentace1.adoc
			# Check all visible pixels for whether they're zero crossing points. Check in all directions horizontal, vertical and both diagonal.
			if pixels[y, x] > 0:
				up = pixels[y + 1, x]
				down = pixels[y - 1, x]
				if (((up < 0 and down >= 0) or (up >= 0 and down < 0)) and abs(up - down) > treshold):
					edges[y - d, x - d] = value
				else:
					upright = pixels[y + 1, x + 1]
					downleft = pixels[y - 1, x - 1]
					if (((upright < 0 and downleft >= 0) or (upright >= 0 and downleft < 0)) and abs(upright - downleft) > treshold):
						edges[y - d, x - d] = value
					else:
						downright = pixels[y - 1, x + 1]
						upleft = pixels[y + 1, x - 1]
						if (((upleft < 0 and downright >= 0) or (upleft >= 0 and downright < 0)) and abs(upleft - downright) > treshold):
							edges[y - d, x - d] = value
						else:
							left = pixels[y, x - 1]
							right = pixels[y, x + 1]
							if (((right < 0 and left >= 0) or (right >= 0 and left < 0)) and abs(right - left) > treshold):
								edges[y - d, x - d] = value
	return edges


def get_available_memory_options():
	"""Finds how much available memory the system has and provides GUI options accordingly."""


	# First check whether psutil is installed. If not, return some small number that should work on anything better than a potato.
	# 512*512*5B*5 (width*height*5 channels because better safe than sorry *5 times where I create an array/list.) is just about 1.3MB. I really hope nobody will try to run this on Commodore 64.
	try:
		import psutil
		avail = int(psutil.virtual_memory().available)
		if avail > (8192**2 * 5 * 5):
			return ("8192", "4096", "2048", "1024", "512")
		elif (avail > 4096**2 * 5 * 5):
			return ("4096", "2048", "1024", "512")
		elif (avail > 2048**2 * 5 * 5):
			return ("2048", "1024", "512")
		elif (avail > 1024**2 * 5 * 5):
			return ("1024", "512")
		elif (avail > 512**2 * 5 * 5):
			return ("512")
		else:
			return ("128")
	except ImportError:
		return ("512")


# Plugin registration
# This really helped me with choosing the options:
# https://github.com/akkana/gimp-plugins/commit/a1e1fb92e5695564a6a4c5e497c0b95024d18250
register(
	"xl_marr_edge_detect",
	"Marr edge detection",
	"Edge detection based on the Marr-Hildreth algorithm",
	"Krystof Jezek",
	"Krystof Jezek",
	"2019",
	"<Image>/Filters/Edge-Detect/Marr-Hildreth Edge Detector...",
	"RGB*, GRAY*",
	[
		(PF_SLIDER, "gauss_blur", "Blur amount:", 3, (1, 15, 0.5)),
		(PF_SLIDER, "treshold", "Detection treshold:", 0.5, (0.05, 10, 0.05)),
		(PF_RADIO, "desaturate_mode", "Desaturation mode:", DESATURATE_LIGHTNESS,
		 (
			 ("Lightness", 0),
			 ("Luminosity", 1),
			 ("Average", 2),
		 )
		 ),
		(PF_OPTION, "size", "Block size (Higher is faster but uses more memory)",
		 0, get_available_memory_options()),
		(PF_BOOL,   "blur_only", "Only blur", False)
	],
	[],
	marr_edge_detector)

main()
